from db import db
from dataclasses import dataclass
import os
import urllib.parse as up
import psycopg2

from flask import Flask
from flask_restful import Api
from flask_jwt import JWT

from security import authenticate, identity
from resources.user import UserRegister
from resources.item import Item, Items
from resources.store import Store, StoreList

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL", "sqlite:///data.db")
# turn off Flask_SQLAchemy tracker. Leaves the SQLAlcemy tracker in place
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = "nathan"
api = Api(app)

# setup Elephant SQL
up.uses_netloc.append("postgres")
url = up.urlparse(os.environ["DATABASE_URL"])
conn = psycopg2.connect(database=url.path[1:],
user=url.username,
password=url.password,
host=url.hostname,
port=url.port
)


# authenticate and identity are function names from security.py
# JWT creates a new endpoing /auth
jwt = JWT(app, authenticate, identity)

api.add_resource(Store, "/store/<string:store_code>")
api.add_resource(Item, "/item/<string:item_name>")
api.add_resource(Items, "/items")
api.add_resource(UserRegister, "/register")
api.add_resource(StoreList, "/stores")

# from db import db
# db.init_app(app)

if __name__ == "__main__":
    # sqlalchemy must be imported here to ensure it is only imported once
    # at run time and not each time app.py is called/imported
    db.init_app(app)
    app.run(debug=True)
