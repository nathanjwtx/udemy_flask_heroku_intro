from unicodedata import name


class User():
    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age


bob = User('bob', 25)
nathy = User('nathy', 50)

all_users = [bob, nathy]

users = {u.name: u for u in all_users}

for c in users:
    print(c)

print(users['bob'].age)
