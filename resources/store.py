from flask_restful import Resource, reqparse
from models.store_model import StoreModel


class Store(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument("store_name", type=str, required=True, help='Cannot be blank')
    
    def get(self, store_code):
        store = StoreModel.find_by_name(store_code)
        if store:
            return store.json()
        return {"message": "Store not found"}, 404

    def post(self, store_code):
        if StoreModel.find_by_name(store_code):
            return {"message": f"{store_code} already exists"}, 400

        data = Store.parser.parse_args()

        store = StoreModel(store_code, data['store_name'])

        try:
            store.save_to_db()
        except:
            return {"message": "oops, something went wrong"}, 500

        return store.json(), 201

    def delete(self, short_code):
        store = StoreModel.find_by_name(short_code)
        if store:
            store.delete_from_db()
        return {"message": f"{short_code} has been deleted"}


class StoreList(Resource):
    def get(self):
        return {"Stores": [store.json() for store in StoreModel.query.all()]}
