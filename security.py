import hmac
from models.user_model import UserModel

def authenticate(username, password):
    print(username)
    user = UserModel.find_by_username(username)
    # if user and safe_str_cmp(user.password, password): # safe_str_cmp removed
    # from werkzeug so hmac.compare used instead
    if user and hmac.compare_digest(user.password, password):
        return user


def identity(payload):
    user_id = payload["identity"]
    return UserModel.find_by_id(user_id)
